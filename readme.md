# setup
```
docker compose up -d
http://localhost:8080
```

## db hostnames
```
percona
mysql
```

## user
```
user: root
pass: admin
```

## delete sql
```
delete pv.*, pt2.*, pt.*, psk.*, pr.*, pp2.*, pp.*, po.*, pm.*, pcfs.*, pcst.*, pcsap.*, pcs2.*, pcs.*, pct.*, pc.*, p.* from product p
left join product_category pc on p.id = pc.product_id 
left join product_category_tree pct on p.id = pct.product_id 
left join product_configurator_setting pcs on p.id = pcs.product_id 
left join product_cross_selling pcs2 on p.id = pcs2.product_id 
left join product_cross_selling_assigned_products pcsap on p.id = pcsap.product_id 
left join product_cross_selling_translation pcst on pcs2.id = pcst.product_cross_selling_id 
left join product_custom_field_set pcfs on p.id = pcfs.product_id 
left join product_media pm on p.id = pm.product_id 
left join product_option po on p.id = po.product_id 
left join product_price pp on p.id = pp.product_id 
left join product_property pp2 on p.id = pp2.product_id 
left join product_review pr on p.id = pr.product_id 
left join product_search_keyword psk on p.id = psk.product_id 
left join product_tag pt on p.id = pt.product_id 
left join product_translation pt2 on p.id = pt2.product_id 
left join product_visibility pv on p.id = pv.product_id;
```